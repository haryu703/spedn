# Release 0.1.5

* fix bug with final `else` statement generating unnecessary `OP_NIP`.

# Release 0.1.4

* fix bug with `fst` and `snd` functions dupicationg `OP_SPLIT`

# Release 0.1.3

* fix bug with stack tracking after `if` [thanks to Jonathan Silverblood for reporting]
* use Schnorr signatures in `SigningContext.signData()`
